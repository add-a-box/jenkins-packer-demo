#!/bin/bash



# Usage: ./run-tests-remote.sh



set -euo pipefail



vmx_file=$(cat output_vmx_file | sed 's/C:\//\/c\//')

mac=$(cat $vmx_file | grep "ethernet0.generatedAddress" | cut -d ' ' -f3 | sed 's/"//g' | head -n1)

# ip=$(cat "/c/ProgramData/VMware/vmnetdhcp.leases" | grep -B3 "${mac}" | head -n1 | cut -d ' ' -f2)

ip=$(cat "/var/db/vmware/vmnet-dhcpd-vmnet8.leases" | grep -B3 "${mac}" | head -n1 | cut -d ' ' -f2)



echo "mac=${mac}"

echo "SSH_HOST=${ip}"



export SSH_USER=vagrant

export SSH_PASSWORD=vagrant

export SSH_HOST="${ip}"



rake spec

