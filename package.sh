#!/bin/bash

# This script expects packer build to produce a local file with the resulting .vmx file
VMX_FILE_DESCRIPTOR="output_vmx_file"
VMX_FILE=$(<${VMX_FILE_DESCRIPTOR})

echo "packaging into an ova..."
ovftool --overwrite --sourceType="vmx" ${VMX_FILE} ${PACKER_OUTPUT_DIR}/centos-7.3.ova

echo "cleaning up vmx..."
VMX_DIR=$(dirname "${VMX_FILE}")
rm -rf ${VMX_DIR}
