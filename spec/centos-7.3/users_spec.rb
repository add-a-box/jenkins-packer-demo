require 'spec_helper'

describe user('root') do
  it { should exist }
  it { should have_home_directory '/root' }
end
