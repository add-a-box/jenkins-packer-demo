require 'spec_helper'

describe service('nginx') do
  it { should be_running.under('systemd') }
end

describe file('/usr/share/nginx/html') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'root' }
end

describe command('curl -sL -w "%{http_code}" "localhost:80" -o /dev/null') do
  its(:stdout) { should match '200' }
end
